//
//  AboutViewController.swift
//  WordPressBlogApp
//
//  Created by Weiping Xing on 7/27/16.
//  Copyright © 2016 Focused on Fit LLC. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    let appUrl = "https://itunes.apple.com/us/app/prepping-directory/id1131115596?ls=1&mt=8"

    let data = [TwoTexts(text0:"Prepper's Directory", text1:"Articles for Preppers"),
                TwoTexts(text0:"Build Version", text1:"Version 1.1"),
                TwoTexts(text0:"Developer", text1:"The App Four Group LLC"),
                TwoTexts(text0:"Copyright", text1:"Copyright 2016 The App Four Group LLC. All rights reserved"),
                TwoTexts(text0:"Tell a Friend", text1:"Tell your friend aoub this app"),
                TwoTexts(text0:"Rate & Review", text1:"Rate and review this app if you think it is awesome")]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "About"
//        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "AboutCell")
        self.tableView.registerNib(UINib(nibName: "AboutTableViewCell", bundle: nil), forCellReuseIdentifier: "AboutCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("AboutCell", forIndexPath: indexPath)
        
        cell.textLabel!.text = data[indexPath.row].text0
        cell.detailTextLabel!.text = data[indexPath.row].text1
        
        if indexPath.row > 3 {
            cell.accessoryType = .DetailDisclosureButton
        } else {
            cell.selectionStyle = .None
        }
        
        return cell
    }

    struct TwoTexts {
        var text0 :String
        var text1 :String
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 4 {
            let activityViewController = UIActivityViewController(activityItems: ["Prepper's Directory\n" + appUrl], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.popoverPresentationController?.sourceRect = CGRect(x: CGRectGetMidX(view.bounds), y: CGRectGetMidY(view.bounds), width: 0, height: 0)
            activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0) //.Down
            presentViewController(activityViewController, animated: true, completion: {})
//            displayShareSheet("Prepper's Directory\n" + appUrl)
        }
        
        if indexPath.row == 5 {
            UIApplication.sharedApplication().openURL(NSURL(string : appUrl)!)
        }
    }
    
    func displayShareSheet(shareContent:String) {
        let activityViewController = UIActivityViewController(activityItems: [shareContent as NSString], applicationActivities: nil)
        presentViewController(activityViewController, animated: true, completion: {})
    }
}
