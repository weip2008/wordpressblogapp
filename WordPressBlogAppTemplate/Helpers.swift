import UIKit

// Replaces html entities
extension String {
    func htmlEncodedString() -> String?  {
        guard let encodedData = self.dataUsingEncoding(NSUTF8StringEncoding) else {
            return nil
        }
        
        let attributedOptions : [String: AnyObject] = [
            NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
            NSCharacterEncodingDocumentAttribute: NSUTF8StringEncoding
        ]
        var attributedString: NSAttributedString?
        do {
            attributedString = try NSAttributedString(data: encodedData, options: attributedOptions, documentAttributes: nil)
        } catch let error as NSError {
            print(error)
        }
        
        return attributedString?.string
    }
}

func toFormattedString(dateString:String) -> String {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    
    if let date = dateFormatter.dateFromString(dateString) {
        dateFormatter.locale = NSLocale.currentLocale()
        dateFormatter.dateStyle = NSDateFormatterStyle.LongStyle
        dateFormatter.timeStyle = .NoStyle
        let formattedDate = dateFormatter.stringFromDate(date)
        
        return formattedDate

    }
    
    return ""
}
