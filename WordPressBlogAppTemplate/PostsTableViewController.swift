import UIKit
import iAd
import GoogleMobileAds
import SideMenu

class PostsTableViewController: UITableViewController, ADBannerViewDelegate {
    
    @IBOutlet var footerLoadingView:FooterLoadingView!
    @IBOutlet var footerLabel: UILabel!
    
    var postsArray: [Posts] = Array()
    var currentPage: Int = 1
    var totalPosts: Int = 0
    var totalPages: Int = 0
    var adView: ADBannerView?
    var didAnimateCell:[NSIndexPath: Bool] = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: #selector(PostsTableViewController.getLatestPosts(_:)), forControlEvents: UIControlEvents.ValueChanged)
        self.refreshControl?.beginRefreshing()
      
        getBlogPosts(currentPage)
        
        footerLoadingView.hidden = true
        
        // Set preferences
        self.title = Settings.Strings.siteName
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        navigationController?.navigationBar.tintColor = Settings.Colors.postTableViewControllerNavigationBarTintColor
        navigationController?.navigationBar.barTintColor = Settings.Colors.postTableViewControllerNavigationBarBackgroundColor
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: Settings.Colors.postTableViewControllerNavigationTitleColor]
        navigationController?.navigationBar.translucent = true
        
//        let sysAction = UIBarButtonItem(barButtonSystemItem: .Action, target: self, action: #selector(addTapped))
//        navigationItem.rightBarButtonItem = sysAction
        
        if Settings.Bools.showAds {
            // Set up ads
            adView = ADBannerView(adType: ADAdType.Banner)
            if (adView != nil) {
                adView!.delegate = self
                self.tableView.tableHeaderView = adView
            }
        }
        
        setupBannerView()
        UIApplication.sharedApplication().keyWindow?.addSubview(bannerView)
        
        setupSideMenu()
    }

    func getLatestPosts(sender:AnyObject) {
         postsArray.removeAll()
         getBlogPosts(1)
    }
    
    func getBlogPosts(page:Int) {
        
        let baseURL = NSURL(string: Settings.Strings.website)
        guard let getPostsURL = NSURL(string: "wp-json/wp/v2/posts?_embed&page=\(page)", relativeToURL: baseURL) else { return }
        let sessionConfig = NSURLSessionConfiguration.defaultSessionConfiguration()
        let sharedSession = NSURLSession(configuration: sessionConfig)

        let downloadPosts: NSURLSessionDownloadTask = sharedSession.downloadTaskWithURL(getPostsURL, completionHandler:{ (location: NSURL?, response: NSURLResponse?, error: NSError?) -> Void in
            
            if (error == nil) {
                
                guard let url = location else {  self.refreshControl?.endRefreshing(); self.showErrorMessage(); return }
                guard let dataObject = NSData(contentsOfURL: url) else {  self.refreshControl?.endRefreshing(); self.showErrorMessage(); return }
                guard let httpHeaders = response as? NSHTTPURLResponse else {  self.refreshControl?.endRefreshing(); self.showErrorMessage(); return }
                guard let headerDict = httpHeaders.allHeaderFields as Any as? NSDictionary else {  self.refreshControl?.endRefreshing(); self.showErrorMessage(); return }
                guard let myTotalPostsString: String = headerDict["X-WP-Total"] as? String else {  self.refreshControl?.endRefreshing(); self.showErrorMessage(); return }
                guard let myTotalPagesString: String = headerDict["X-WP-TotalPages"] as? String else {  self.refreshControl?.endRefreshing(); self.showErrorMessage(); return }
               
                self.totalPosts = Int(myTotalPostsString) ?? 0
                self.totalPages = Int(myTotalPagesString) ?? 0
                
                guard let blogPostArray: NSArray = (try! NSJSONSerialization.JSONObjectWithData(dataObject, options: [])) as? NSArray else { return }
                
                if (blogPostArray.count > 0) {
                    for blogPost in blogPostArray as! [NSDictionary] {
                    
                        let newPost = Posts(blogPost: blogPost)
                        self.postsArray.append(newPost)
                    
                    }
                
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        
                        self.tableView.reloadData()
                        if ((self.refreshControl) != nil) {
                            self.refreshControl?.endRefreshing()
                         }
                        self.footerLoadingView.hidden = true
                  
                    })
                } else {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.refreshControl?.endRefreshing()
                        self.footerLoadingView.activityIndicator.hidden = true
                        self.footerLabel.text = Settings.Strings.noMorePostsText
                        
                    })
                }
            } else {
                self.refreshControl?.endRefreshing()
                self.showErrorMessage()
            }
            
        })
        
        downloadPosts.resume()
    }

    func showErrorMessage() {
            let messageLabel = UILabel(frame: CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height))
            messageLabel.text = Settings.Strings.noConnectionText
            messageLabel.textColor = Settings.Colors.errorMessageColor
            messageLabel.numberOfLines = 0
            messageLabel.textAlignment = .Center
            messageLabel.font.fontWithSize(20)
            messageLabel.sizeToFit()
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.tableView.backgroundView = messageLabel
            })
    }

    
    func loadMorePosts() {
        self.footerLoadingView.hidden = false
        self.footerLabel.text = Settings.Strings.loadingPostsText
        
        if currentPage < totalPages {
            currentPage = currentPage + 1
            getBlogPosts(currentPage)
        } else {
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.refreshControl?.endRefreshing()
                self.footerLoadingView.activityIndicator.hidden = true
                self.footerLabel.text = Settings.Strings.noMorePostsText
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - TableView

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.postsArray.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifiers.postCell.rawValue , forIndexPath: indexPath) as! PostTableViewCell
        
        let post: Posts = self.postsArray[indexPath.row]
        cell.setPost(post)
        
        if (indexPath.row == self.postsArray.count - 1) {
            loadMorePosts()
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if didAnimateCell[indexPath] == nil || didAnimateCell[indexPath]! == false {
            didAnimateCell[indexPath] = true
            let view = cell.contentView
        
            view.layer.opacity = 0.6
        
            UIView.animateWithDuration(0.4) {
            view.layer.transform = CATransform3DIdentity
            view.layer.opacity = 1
        }
      }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == SegueIdentifiers.showPostDetails.rawValue {
            if let row = tableView.indexPathForSelectedRow?.row {
                let destinationController = segue.destinationViewController as! PostDetailTableViewController
                destinationController.post = self.postsArray[row]
            }
        }
    }

    func addTapped()  {
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let action0 = UIAlertAction(title: "Prepping Sites", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            let vc = ViewController()
            self.navigationController?.pushViewController(vc, animated: true)
            self.bannerView.hidden = false
        })
        
        let actionHelp = UIAlertAction(title: "Questions and Help", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            guard let url = NSURL(string: "http://www.preppersatlas.com/qa.html") else { return }
            UIApplication.sharedApplication().openURL(url)
            print("http://www.preppersatlas.com/qa.html")
        })
        
        let actionAbout = UIAlertAction(title: "About", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.navigationController?.pushViewController(AboutViewController(), animated: true)
            self.bannerView.hidden = false
        })
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancel")
        })
        
        optionMenu.addAction(action0)
        optionMenu.addAction(actionHelp)
        optionMenu.addAction(actionAbout)
        optionMenu.addAction(actionCancel)
        
//        handle iPad error
        if let popoverPresentationController = optionMenu.popoverPresentationController {
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = CGRect(x: CGRectGetMidX(view.bounds) - 150, y: CGRectGetMidY(view.bounds), width: 0, height: 0)
        }
        
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    // MARK: - google ad view
    
    let bannerView: GADBannerView! = GADBannerView(frame: CGRectMake(20, UIScreen.mainScreen().bounds.size.height - 44.0, UIScreen.mainScreen().bounds.size.width - 30, 44))
    
    func setupBannerView() {
        self.bannerView.adUnitID = AppDelegate.kBannerAdUnitID
        self.bannerView.rootViewController = self
        self.bannerView.loadRequest(GADRequest())
        self.bannerView.backgroundColor = UIColor.clearColor()
    }
    
    override func viewWillAppear(animated: Bool) {
        if !bannerView.hidden {
            bannerView.hidden = true
        }
    }
    
    // MARK: - sideMenu
    
   
    private func setupSideMenu() {
        SideMenuManager.menuLeftNavigationController = storyboard!.instantiateViewControllerWithIdentifier("LeftMenuNavigationController") as? UISideMenuNavigationController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        SideMenuManager.menuPresentMode = .ViewSlideInOut
        SideMenuManager.menuShadowOpacity = 0.1
    }
    
//    func presentSideMenu() {
//        presentViewController(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
//    }
    
    @IBAction func menuBarAction(sender: UIBarButtonItem) {
        presentViewController(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
}
