//
//  DataSource.swift
//  PreppingDirectory
//
//  Created by Weiping Xing on 11/26/15.
//  Copyright © 2016 APP FOUR GROUP LLC. All rights reserved.
//

import Foundation


class BookModel {
    let fm3_06 = ["Cover", "Chapter 1", "Chapter 2", "Chapter 3", "Chapter 4", "Chapter 5", "Chapter 6", "Chapter 7", "Chapter 8", "Chapter 9", "Chapter 10", "Appendix A", "Appendix B", "Appendix C", "Appendix D"]
    
    let fm3_11_4 = ["Cover", "Chapter 1", "Chapter 2", "Chapter 3", "Chapter 4", "Chapter 5", "Chapter 6", "Chapter 7",  "Appendix A", "Appendix B", "Appendix C", "Appendix D",  "Appendix E", "Appendix F", "Appendix G", "Appendix H",  "Appendix I", "Appendix J"]
    
    let fm3_19_15 = ["Cover", "Chapter 1", "Chapter 2", "Chapter 3", "Chapter 4", "Chapter 5", "Chapter 6", "Chapter 7", "Chapter 8", "Appendix A", "Appendix B", "Appendix C", "Appendix D"]
    
    let fm3_34_39 = ["Cover", "Chapter 1", "Chapter 2", "Chapter 3", "Chapter 4", "Chapter 5", "Chapter 6", "Chapter 7", "Appendix A", "Appendix B", "Appendix C", "Appendix D",  "Appendix E", "Appendix F"]
    
    let fm21_76_1 = ["Cover", "Chapter 1", "Chapter 2", "Chapter 3", "Chapter 4", "Chapter 5", "Chapter 6", "Chapter 7", "Chapter 8", "Chapter 9", "Appendix A", "Appendix B"]
    
    let resourcePath = NSBundle.mainBundle().pathsForResourcesOfType("", inDirectory: "Resource")

    let booksMap = ["FM3 06": "fm3_06","FM3 11 4": "fm3_11_4","FM3 19 15": "fm3_19_15","FM3 34 39": "fm3_34_39", "FM21 76 1": "fm21_76_1"]

    let bookNameKeys = ["FM3 06", "FM3 11 4", "FM3 19 15", "FM3 34 39", "FM21 76 1"]
    
    let bookList = ["fm3_06", "fm3_11_4", "fm3_19_15", "fm3_34_39", "fm21_76_1"]
    
    func displayBookList() -> [String]  {
        let rtn = self.bookList.map { (book) -> String in
            return book.uppercaseString
        }
        
        return rtn
    }
    
    let books = ["FM3 06": ["cover.htm", "indexsection.htm", "preface.htm", "chapter-1.htm", "chapter-2.htm", "chapter-3.htm", "chapter-4.htm", "chapter-5.htm", "chapter-6.htm", "chapter-7.htm", "chapter-8.htm", "chapter-9.htm", "chapter-10.htm", "appendix-a.htm", "appendix-b.htm", "appendix-c.htm", "appendix-d.htm", "contents.htm", "references.htm", "sourcenotes.htm", "glossary.htm"],
        
        "FM3 11 4":["cover.htm", "foreward.htm", "preface.htm", "chapter-1.htm", "chapter2.htm", "chapter3.htm", "chapter4.htm", "chapter5.htm", "chapter6.htm", "chapter7.htm", "appendix-a.htm", "appendix-b.htm", "appendix-c.htm", "appendix-d.htm", "appendix-e.htm", "appendix-f.htm", "appendix-g.htm", "appendix-h.htm", "appendix-i.htm", "appendix-j.htm", "endindex.htm", "esummary.htm", "glossary.htm", "particpiants.htm", "references.htm", "terms.htm", "toc.htm"],
        
        "FM3 19 15":["cover.htm", "index.htm", "preface.htm", "chapter-1.htm", "chapter-2.htm", "chapter-3.htm", "chapter-4.htm", "chapter-5.htm", "chapter-6.htm", "chapter-7.htm", "chapter-8.htm", "appendix-a.htm", "appendix-b.htm", "appendix-c.htm", "appendix-d.htm", "bibliography.htm", "contents.htm", "glossary.htm"],
        
        "FM3 34 39":["cover.htm", "indexfile.htm", "preface.htm", "chapter-1.htm", "chapter-2.htm", "chapter-3.htm", "chapter-4.htm", "chapter-5.htm", "chapter-6.htm", "chapter-7.htm", "appendix-a.htm", "appendix-b.htm", "appendix-c.htm", "appendix-d.htm", "appendix-e.htm", "appendix-f.htm", "contents.htm", "glossary.htm", "references.htm"],
        
        "FM21 76 1":["cover.htm", "chapter-1.htm", "chapter-2.htm", "chapter-3.htm", "chapter-4.htm", "chapter-5.htm", "chapter-6.htm", "chapter-7.htm", "chapter-8.htm", "chapter-9.htm", "appendix-a.htm", "appendix-b.htm", "checklist.htm", "codeofcontent.htm", "content.htm"]]
    
    func getBooksKeys() -> [String] {
        return Array(books.keys)
    }
    
    func getBooksValues() -> [[String]] {
        return Array(books.values)
    }
 
    func getBooksKey(Idx: Int) -> String {
        return Array(books.keys)[Idx]
    }
    
    func getBooksValue(Idx: Int) -> [String] {
        return Array(books.values)[Idx]
    }
    
    func getChapterLink(keyIdx: Int, valueIdx: Int) -> String {
        let values = getBooksValue(keyIdx)
        let fileName = values[valueIdx]
        return self.resourcePath[keyIdx] + "/" + fileName
    }
}

// use Assets

class Assets {
    let MODEL_FILE = "texas.json";
    //    private Context context;
    var fileName: String = "";
    var bookList = [Book]()
    
    func loadJsonFromAsset() {
//        if (fileName.isEmpty) {
//            fileName = MODEL_FILE;
//        }
        
        
        let jsonPath = NSBundle.mainBundle().pathForResource("texas", ofType: "json")
        let jsonData = NSData(contentsOfFile: jsonPath!)
        
//        let json = JSON(data:jsonData!)
        
        let json = try? NSJSONSerialization.JSONObjectWithData(jsonData!, options: [])
        let books = json!["books"] as? NSArray
//        var bookList = [Book]()

        for aBook in books! {
            let bookName = aBook["bookName"] as! String
            let bookDir = aBook["bookDir"] as! String
            let chapters = self.parseChapter(aBook as! NSDictionary)
            bookList.append(Book(bookName: bookName,
                                  bookDir: bookDir,
                                 chapters: chapters))
//            print(bookName)
        }

    }
    
    func parseChapter(aBook:NSDictionary) -> [Chapter] {
        var chapterList = [Chapter]()
        let chapters = aBook["chapters"] as? NSArray
        for item in chapters! {
            let showText = item["showText"] as! String
            let showText1 = item["showText1"] as! String
            let fileName = item["fileName"] as! String
            chapterList.append(Chapter(showText: showText, showText1: showText1, fileName: fileName))
        }
        
        return chapterList
    }
}

struct Book {
    var bookName = ""
    var bookDir = ""
    var chapters = [Chapter]()
}

struct Chapter {
    var showText = ""
    var showText1 = ""
    var fileName = ""
}
