//
//  SideMenuTableView.swift
//  WordPressBlogApp
//
//  Created by Weiping Xing on 9/6/16.
//  Copyright © 2016 Focused on Fit LLC. All rights reserved.
//

import Foundation
import SideMenu

class SideMenuTableView: UITableViewController {
    
    let tableData = ["Prepers books", "Prepping Sites", "Questions and Help", "About"]
//    var actions = [] // will try add action in array.
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.row {
        case 0:
            self.navigationController?.pushViewController(ViewController(), animated: true)
            
        case 1:
            self.navigationController?.pushViewController(ViewController(), animated: true)
            
        case 2:
            guard let url = NSURL(string: "http://www.preppersatlas.com/qa.html") else { return }
            UIApplication.sharedApplication().openURL(url)
            print("http://www.preppersatlas.com/qa.html")
            
        case 3:
            self.navigationController?.pushViewController(AboutViewController(), animated: true)
            
        default:
             self.navigationController?.pushViewController(AboutViewController(), animated: true)
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SideMenuTableViewCell", forIndexPath: indexPath)
        let titleText = tableData[indexPath.row]
        cell.textLabel?.text = titleText
        
        return cell
    }
}
