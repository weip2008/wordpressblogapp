import UIKit
import WebKit
import iAd

class PostDetailTableViewController: UITableViewController, WKNavigationDelegate, ADBannerViewDelegate {

    var post:Posts?
    var footerView = WKWebView()
    var contentString = "Could not load content"
    var adView: ADBannerView?
    var shareBarButton = UIBarButtonItem()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup navigation buttons
        shareBarButton =  UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Action, target: self, action: #selector(PostDetailTableViewController.presentShareSheet))
        navigationItem.rightBarButtonItems = [shareBarButton]

        // Load CSS to style post content
        if let cssPath = NSBundle.mainBundle().URLForResource("default", withExtension: "css") {
            do {
                let cssString = try NSString(contentsOfURL: cssPath, encoding: NSUTF8StringEncoding)
                if let content = post?.postContent {
                    contentString = (cssString as String) + content
                }
                
            } catch _ as NSError {
                
            }
        }

        footerView.loadHTMLString(contentString, baseURL: nil)
        footerView.navigationDelegate = self
        footerView.scrollView.scrollEnabled = false
        footerView.frame.size.height = view.frame.size.height
        
        tableView.tableFooterView = footerView
        tableView.estimatedRowHeight = 100.0
        
        if Settings.Bools.showAds {
            // Set up ads
            adView = ADBannerView(adType: ADAdType.Banner)
            if (adView != nil) {
                adView!.delegate = self
                self.tableView.tableHeaderView = adView
            }
        }
        
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: WebKit
    
    func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        
        webView.evaluateJavaScript("document.height", completionHandler: { (contentHeight, Error) -> Void in
            if let height = contentHeight as? CGFloat {
                self.footerView.frame.size.height = height
                self.tableView.tableFooterView = self.footerView
            }
        })
    }
    
    func webView(webView: WKWebView, decidePolicyForNavigationAction navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .LinkActivated {
            guard let url = navigationAction.request.URL else {
                 decisionHandler(.Cancel)
                return
            }
            UIApplication.sharedApplication().openURL(url)
             decisionHandler(.Cancel)
            return
        }
            decisionHandler(.Allow)
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 0
        case 1: return 1
        default: return 0
        }
    }

    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0: if let _ = post?.postFeaturedImageURL {
                    return 200
                } else {
                    return 0
                }
        default: return 0
        }
        
    }
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
       
        case 0:
            let reuseIdentifier = "header"
            if let stringURL = post?.postFeaturedImageURL {
                let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier) as! PostDetailHeaderTableViewCell
                if let url = NSURL(string: stringURL)  {
                    cell.featuredImage.kf_setImageWithURL(url, placeholderImage: UIImage(named:"Placeholder"))
                    cell.featuredImage.contentMode = UIViewContentMode.ScaleAspectFill
                    return cell
                }
            }
            // There is no Image
            return nil
            
        default: return nil
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCellWithIdentifier("meta", forIndexPath: indexPath) as! PostDetailMetaTableViewCell
        
        cell.postTitleLabel.textColor = Settings.Colors.postDetailTableViewCellTitleColor
        cell.postDetailsLabel.textColor = Settings.Colors.postDetailTableViewCellMetaColor
        cell.postTitleLabel.text = post?.postTitle?.htmlEncodedString()

        guard let date = post?.postDate else {
            cell.postDetailsLabel.text = ""
            return cell
        }
        guard let author = post?.postAuthor else {
            cell.postDetailsLabel.text = ""
            return cell
        }
        cell.postDetailsLabel.text = "\(author) - \(toFormattedString(date))"
        
        return cell
    }
    
    override func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
  
   // Share sheet

    func presentShareSheet() {

        var objectsToShare = [AnyObject]()

        // Set Post title
        if let textToShare = post?.postTitle?.htmlEncodedString() {

            objectsToShare.append(textToShare)

        }

        // Set URL
        if let urlString = post?.postURL {
            if let url = NSURL(string: urlString) {
                objectsToShare.append(url)
            }
        }

        // Set image
        if let stringURL = post?.postFeaturedImageURL {
            if let url = NSURL(string: stringURL)  {
                let imageView = UIImageView()
                imageView.kf_setImageWithURL(url, placeholderImage: nil)
                if let image = imageView.image {
                    objectsToShare.append(image)
                }

            }
        }

        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

        activityVC.excludedActivityTypes = [UIActivityTypeAssignToContact, UIActivityTypePostToVimeo]
        activityVC.popoverPresentationController?.barButtonItem = shareBarButton
        activityVC.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.Up

        presentViewController(activityVC, animated: true, completion: nil)

    }
  
}
