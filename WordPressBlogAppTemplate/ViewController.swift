//
//  ViewController.swift
//  WordPressBlogAppTemplate
//
//  Created by Weiping Xing on 7/24/16.
//  Copyright © 2016 Focused on Fit LLC. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var bookList = [Book]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        self.navigationItem.title = "Prepper's Direcory"
        prepareData()
    }
    
    func prepareData() {
        let assets = Assets()
        assets.loadJsonFromAsset();
        bookList = assets.bookList;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        let titleText = bookList[(indexPath as NSIndexPath).row].bookName
        cell.textLabel?.text = titleText
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
            let vc = DetailViewController()
            vc.book = bookList[indexPath.row]
            
            navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
            self.navigationController?.pushViewController(vc, animated: true)
    }
}
