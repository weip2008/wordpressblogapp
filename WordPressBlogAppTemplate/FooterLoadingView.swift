import UIKit

class FooterLoadingView: UIView {

    @IBOutlet var activityIndicator:UIActivityIndicatorView!

    override var hidden:Bool {
        get {
            return super.hidden
        }
        set(hidden) {
            super.hidden = hidden
            if (activityIndicator != nil) {
                if hidden {
                    self.activityIndicator.stopAnimating()
                } else {
                    self.activityIndicator.startAnimating()
                }
            }
        }
    }

}

