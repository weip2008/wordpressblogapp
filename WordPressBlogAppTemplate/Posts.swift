import Foundation
import UIKit

struct Posts {
     var postID: Int?
     var postContent: String?
     var postDate: String?
     var postFeaturedImageURL: String?
     var postLink: String?
     var postModified: String?
     var postTitle: String?
     var postAuthor: String?
     var postURL: String?
    
    init(blogPost:NSDictionary) {
        // Pre beta 13
        let post = blogPost as NSDictionary
        if let featuredMedia = post["_embedded"]?["https://api.w.org/featuredmedia"]??[0]["media_details"]??["sizes"]??["\(Settings.Strings.featuredImageSize)"]??["source_url"] {
            if let url = featuredMedia as? String {
                postFeaturedImageURL = url
            }
        }

        // Support CURIE change in beta 13+
        if let featuredMedia = post["_embedded"]?["wp:featuredmedia"]??[0]["media_details"]??["sizes"]??["\(Settings.Strings.featuredImageSize)"]??["source_url"] {
            if let url = featuredMedia as? String {
                postFeaturedImageURL = url
            }
        }

    
        postAuthor = post["_embedded"]?["author"]??[0]["name"] as? String
        postID = post["id"] as? Int
        postDate = post["date_gmt"] as? String
        postLink = post["link"] as? String
        postModified = post["modified"] as? String
        postURL = post["link"] as? String
        
        if let contentDictionary: NSDictionary = post["content"] as? NSDictionary {
            if let content = getRenderedContent(contentDictionary) {
                postContent = content
            }
        }

        if let titleDictionary: NSDictionary = post["title"] as? NSDictionary {
            if let title = getRenderedContent(titleDictionary) {
                postTitle = title
            }
        }
        
    }
    
    func getRenderedContent(renderedDict:NSDictionary) -> String? {
       
        if let renderedValue = renderedDict["rendered"] as? String {
            return renderedValue
        }
        return nil
    }
    
}
