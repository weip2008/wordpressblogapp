import UIKit
import Kingfisher

class PostTableViewCell: UITableViewCell {

    @IBOutlet var mainView: UIView!
    @IBOutlet var postTitleLabel: UILabel!
    @IBOutlet var featuredImage: UIImageView!
    
    func setPost(post:Posts) {
       
        mainView.layer.cornerRadius = 10
        mainView.layer.masksToBounds = true

        postTitleLabel.text = post.postTitle?.htmlEncodedString()

        if let stringURL = post.postFeaturedImageURL {
            if let url = NSURL(string: stringURL)  {
                self.featuredImage.kf_setImageWithURL(url, placeholderImage: UIImage(named:"Placeholder"))
            }
        } else {
            self.featuredImage.image = UIImage(named:"Placeholder")

        }

    }
}
