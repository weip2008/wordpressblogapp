//
//  DetailViewController.swift
//  WordPressBlogAppTemplate
//
//  Created by Weiping Xing on 7/24/16.
//  Copyright © 2016 Focused on Fit LLC. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var chapters:[Chapter]?
    
    var book: Book? {
        didSet {
            // Update the view.
            self.chapters = book!.chapters
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = self.book?.bookName
        
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "DetailCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let countNumber = self.chapters?.count {
            return countNumber
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("DetailCell", forIndexPath: indexPath)
        let titleText = self.chapters?[(indexPath as NSIndexPath).row].showText
        cell.textLabel!.text = titleText
        
        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        guard let url = NSURL(string: (self.chapters?[indexPath.row].fileName)!) else { return }
        UIApplication.sharedApplication().openURL(url)
    }
}
