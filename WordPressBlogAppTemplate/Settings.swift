import UIKit

struct Settings {

    struct Strings {
        static let website = "http://www.preppersatlas.com"
        //"http://wired.com"
        static let siteName = "Prepper's Directory"
        static let featuredImageSize = "small"
        static let noMorePostsText = "That's All Folks"
        static let noConnectionText = "Please Check Your Internet Connection"
        static let loadingPostsText = "Loading Posts"
    }
    struct Colors {
        // Custom Colors
        static let myBlueColor = UIColor(red: 78.0/255.0, green: 178.0/255.0, blue: 207.0/255.0, alpha: 1.0)

        // Elements
        static let caption = UIColor.blackColor().colorWithAlphaComponent(0.8)
        static let title = UIColor.blackColor()
        static let errorMessageColor = UIColor.blackColor()
        static let postTableViewControllerNavigationBarTintColor = UIColor.blackColor()
        static let postTableViewControllerNavigationBarBackgroundColor = UIColor.whiteColor()
        static let postTableViewControllerNavigationTitleColor = Colors.title
        static let postDetailTableViewCellTitleColor = Colors.title
        static let postDetailTableViewCellMetaColor = Colors.caption
       
    }
    struct Bools {
        static let showAds = false
    }
        
}

enum CellIdentifiers: String {
    case postCell
}

enum SegueIdentifiers: String {
    case showPostDetails
}

enum Errors : ErrorType {
    case CouldNotConnect, Unknown
}